package edu.upenn.cis.cis455.m2.server;

import edu.upenn.cis.cis455.ServiceFactory;
import edu.upenn.cis.cis455.WebServiceController;
import edu.upenn.cis.cis455.handlers.Route;
import edu.upenn.cis.cis455.m2.server.interfaces.Request;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.Socket;
import java.nio.file.Paths;

import static edu.upenn.cis.cis455.TestHelper.getMockSocketWithString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Tests for checking that the right route is registered for requests
 */
public class TestRouteDelegation {

    @Before
    public void setUp() {
        ServiceFactory.getServerInstance().resetInstance();
    }

    @Test
    public void testNoRouteMatch() throws IOException {
        Route route1 = (request, response) -> "hello hello";
        WebServiceController.get("/test/route", route1);

        String reqString =
                "GET /test/ HTTP/1.1\r\n" +
                "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
                "Host: www.cis.upenn.edu\r\n" +
                "Accept-Language: en-us\r\n" +
                "Accept-Encoding: gzip, deflate\r\n" +
                "Cookie: name1=value1;name2=value2;name3=value3\r\n" +
                "Connection: Keep-Alive\r\n\r\n";

        Socket sock = getMockSocketWithString(reqString);
        Request req = ServiceFactory.createRequest(sock, null, false, null, null);
        assertNull(ServiceFactory.getServerInstance().getRoute(req));
    }

    @Test
    public void testSingleRouteMatch() throws IOException {
        Route route1 = (request, response) -> "hello hello";
        WebServiceController.get("/test/route", route1);

        String reqString =
                "GET /test/route HTTP/1.1\r\n" +
                "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
                "Host: www.cis.upenn.edu\r\n" +
                "Accept-Language: en-us\r\n" +
                "Accept-Encoding: gzip, deflate\r\n" +
                "Cookie: name1=value1;name2=value2;name3=value3\r\n" +
                "Connection: Keep-Alive\r\n\r\n";

        Socket sock = getMockSocketWithString(reqString);
        Request req = ServiceFactory.createRequest(sock, null, false, null, null);
        assertEquals(route1, ServiceFactory.getServerInstance().getRoute(req));
    }

    @Test
    public void testWildcard() throws IOException {
        Route route1 = (request, response) -> "hello hello";
        WebServiceController.get("/test/*", route1);

        String reqString =
                "GET /test/route HTTP/1.1\r\n" +
                "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
                "Host: www.cis.upenn.edu\r\n" +
                "Accept-Language: en-us\r\n" +
                "Accept-Encoding: gzip, deflate\r\n" +
                "Cookie: name1=value1;name2=value2;name3=value3\r\n" +
                "Connection: Keep-Alive\r\n\r\n";

        Socket sock = getMockSocketWithString(reqString);
        Request req = ServiceFactory.createRequest(sock, null, false, null, null);
        assertEquals(route1, ServiceFactory.getServerInstance().getRoute(req));
    }

    @Test
    public void testRouteWithNamedParams() throws IOException {
        Route route1 = (request, response) -> "hello hello";
        WebServiceController.get("/test/:name", route1);

        String reqString =
                "GET /test/route HTTP/1.1\r\n" +
                "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
                "Host: www.cis.upenn.edu\r\n" +
                "Accept-Language: en-us\r\n" +
                "Accept-Encoding: gzip, deflate\r\n" +
                "Cookie: name1=value1;name2=value2;name3=value3\r\n" +
                "Connection: Keep-Alive\r\n\r\n";

        Socket sock = getMockSocketWithString(reqString);
        Request req = ServiceFactory.createRequest(sock, null, false, null, null);
        assertEquals(route1, ServiceFactory.getServerInstance().getRoute(req));
    }

    @Test
    public void testPickFirstOfManyPossibleRoutes() throws IOException {
        Route route1 = (request, response) -> "hello hello";
        Route route2 = (request, response) -> "goodbye goodbye";
        WebServiceController.get("/test/*", route1);
        WebServiceController.get("/test/route", route2);

        String reqString =
                "GET /test/route HTTP/1.1\r\n" +
                "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
                "Host: www.cis.upenn.edu\r\n" +
                "Accept-Language: en-us\r\n" +
                "Accept-Encoding: gzip, deflate\r\n" +
                "Cookie: name1=value1;name2=value2;name3=value3\r\n" +
                "Connection: Keep-Alive\r\n\r\n";

        Socket sock = getMockSocketWithString(reqString);
        Request req = ServiceFactory.createRequest(sock, null, false, null, null);
        assertEquals(route1, ServiceFactory.getServerInstance().getRoute(req));
    }
}

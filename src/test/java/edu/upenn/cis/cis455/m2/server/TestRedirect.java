package edu.upenn.cis.cis455.m2.server;

import edu.upenn.cis.cis455.ServiceFactory;
import edu.upenn.cis.cis455.TestHelper;
import edu.upenn.cis.cis455.WebServiceController;
import edu.upenn.cis.cis455.exceptions.HaltException;
import edu.upenn.cis.cis455.m1.server.HttpIoHandler;
import edu.upenn.cis.cis455.m1.server.interfaces.HttpRequestHandler;
import edu.upenn.cis.cis455.m2.server.interfaces.Request;
import edu.upenn.cis.cis455.m2.server.interfaces.Response;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.nio.file.Paths;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestRedirect {

    HttpRequestHandler requestHandler;

    @Before
    public void setUp() {
        requestHandler = ServiceFactory.createRequestHandlerInstance(Paths.get("./www"));
    }

    @Test
    public void testRedirectCalledInRoute() throws IOException {
        WebServiceController.get("/a", (request, response) -> {
            response.redirect("/b");
            return "hello";
        });

        String reqString =
                "GET /a HTTP/1.1\r\n" +
                "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
                "Host: www.cis.upenn.edu\r\n" +
                "Accept-Language: en-us\r\n" +
                "Accept-Encoding: gzip, deflate\r\n" +
                "Cookie: name1=value1;name2=value2;name3=value3\r\n" +
                "Connection: Keep-Alive\r\n\r\n";

        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        Socket s = TestHelper.getMockSocket(
                reqString,
                byteArrayOutputStream);
        Request req = ServiceFactory.createRequest(s, null, false, null, null);
        Response response = ServiceFactory.createResponse();
        try {
            requestHandler.handle(req, response);
        } catch (HaltException e) {
            assertEquals(301, e.statusCode());
        }

    }
}

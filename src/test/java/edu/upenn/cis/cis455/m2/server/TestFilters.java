package edu.upenn.cis.cis455.m2.server;

import edu.upenn.cis.cis455.ServiceFactory;
import edu.upenn.cis.cis455.WebServiceController;
import edu.upenn.cis.cis455.exceptions.HaltException;
import edu.upenn.cis.cis455.handlers.Filter;
import edu.upenn.cis.cis455.m1.server.interfaces.HttpRequestHandler;
import edu.upenn.cis.cis455.m2.server.interfaces.Response;
import edu.upenn.cis.cis455.m2.server.interfaces.Request;
import org.junit.Before;
import org.junit.Test;

import javax.xml.ws.Service;
import java.io.IOException;
import java.net.Socket;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static edu.upenn.cis.cis455.TestHelper.getMockSocketWithString;
import static org.junit.Assert.assertEquals;

public class TestFilters {

    HttpRequestHandler requestHandler;

    @Before
    public void setUp() {
        ServiceFactory.getServerInstance().resetInstance();
        requestHandler = ServiceFactory.createRequestHandlerInstance(Paths.get("./www"));
    }

    @Test
    public void testGetMultipleBeforeFilters() throws IOException {
        Filter filter1 = (request, response) -> {
            request.params("param");
        };
        Filter filter2 = (request, response) -> {
            request.params("other");
        };
        WebServiceController.before(filter1, filter2);
        List<Filter> filters = new ArrayList<>();
        filters.add(filter1);
        filters.add(filter2);

        String reqString =
                "GET /test/ HTTP/1.1\r\n" +
                        "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
                        "Host: www.cis.upenn.edu\r\n" +
                        "Accept-Language: en-us\r\n" +
                        "Accept-Encoding: gzip, deflate\r\n" +
                        "Cookie: name1=value1;name2=value2;name3=value3\r\n" +
                        "Connection: Keep-Alive\r\n\r\n";

        Socket sock = getMockSocketWithString(reqString);
        Request req = ServiceFactory.createRequest(sock, null, false, null, null);

        assertEquals(filters, ServiceFactory.getServerInstance().getBeforeFilters(req));
    }

    @Test
    public void testGetMultipleAfterFilters() throws IOException {
        Filter filter1 = (request, response) -> {
            request.params("param");
        };
        Filter filter2 = (request, response) -> {
            request.params("other");
        };
        WebServiceController.after(filter1, filter2);
        List<Filter> filters = new ArrayList<>();
        filters.add(filter1);
        filters.add(filter2);

        String reqString =
                "GET /test/ HTTP/1.1\r\n" +
                        "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
                        "Host: www.cis.upenn.edu\r\n" +
                        "Accept-Language: en-us\r\n" +
                        "Accept-Encoding: gzip, deflate\r\n" +
                        "Cookie: name1=value1;name2=value2;name3=value3\r\n" +
                        "Connection: Keep-Alive\r\n\r\n";

        Socket sock = getMockSocketWithString(reqString);
        Request req = ServiceFactory.createRequest(sock, null, false, null, null);

        assertEquals(filters, ServiceFactory.getServerInstance().getAfterFilters(req));
    }

    @Test
    public void testFilterSetsRequestAttribute() throws IOException {
        WebServiceController.before((request, response) -> {
            request.attribute("attrib", "val");
        });

        String reqString =
                "GET /test/ HTTP/1.1\r\n" +
                        "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
                        "Host: www.cis.upenn.edu\r\n" +
                        "Accept-Language: en-us\r\n" +
                        "Accept-Encoding: gzip, deflate\r\n" +
                        "Cookie: name1=value1;name2=value2;name3=value3\r\n" +
                        "Connection: Keep-Alive\r\n\r\n";

        Socket sock = getMockSocketWithString(reqString);
        Request req = ServiceFactory.createRequest(sock, null, false, null, null);
        Response response = ServiceFactory.createResponse();

        for (Filter f : ServiceFactory.getServerInstance().getBeforeFilters(req)) {
            try {
                f.handle(req, response);
            } catch (Exception e) {
                /* nothing to do here */
            }
        }
        assertEquals("val", req.attribute("attrib"));
    }

    @Test(expected = HaltException.class)
    public void testFilterHalts() throws IOException {
        WebServiceController.before((request, response) -> {
            WebServiceController.halt();
        });

        String reqString =
                "GET /test/ HTTP/1.1\r\n" +
                        "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
                        "Host: www.cis.upenn.edu\r\n" +
                        "Accept-Language: en-us\r\n" +
                        "Accept-Encoding: gzip, deflate\r\n" +
                        "Cookie: name1=value1;name2=value2;name3=value3\r\n" +
                        "Connection: Keep-Alive\r\n\r\n";

        Socket sock = getMockSocketWithString(reqString);
        Request req = ServiceFactory.createRequest(sock, null, false, null, null);
        Response response = ServiceFactory.createResponse();

        requestHandler.handle(req, response);
    }
}

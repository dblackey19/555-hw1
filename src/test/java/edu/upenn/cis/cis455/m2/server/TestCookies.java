package edu.upenn.cis.cis455.m2.server;

import edu.upenn.cis.cis455.ServiceFactory;
import edu.upenn.cis.cis455.WebServiceController;
import edu.upenn.cis.cis455.m2.server.interfaces.Request;
import org.junit.Test;

import java.io.IOException;
import java.net.Socket;

import static edu.upenn.cis.cis455.TestHelper.getMockSocketWithString;
import static org.junit.Assert.assertEquals;

public class TestCookies {

    @Test
    public void testCookieParsedFromRequest() throws IOException {
        String reqString =
                "GET /test/john/value HTTP/1.1\r\n" +
                        "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
                        "Host: www.cis.upenn.edu\r\n" +
                        "Accept-Language: en-us\r\n" +
                        "Accept-Encoding: gzip, deflate\r\n" +
                        "Cookie: name1=value1;name2=value2;name3=value3\r\n" +
                        "Connection: Keep-Alive\r\n\r\n";

        Socket sock = getMockSocketWithString(reqString);
        Request req = ServiceFactory.createRequest(sock, null, false, null, null);
        assertEquals("value1", req.cookie("name1"));
        assertEquals("value2", req.cookie("name2"));
        assertEquals("value3", req.cookie("name3"));
    }

    @Test
    public void testSessionSetProperly() throws IOException {
        String id = ServiceFactory.createSession();
        String reqString =
                "GET /test/john/value HTTP/1.1\r\n" +
                "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
                "Host: www.cis.upenn.edu\r\n" +
                "Accept-Language: en-us\r\n" +
                "Accept-Encoding: gzip, deflate\r\n" +
                "Cookie: name1=value1;name2=value2;name3=value3;" +"JSESSIONID=" + id + "\r\n" +
                "Connection: Keep-Alive\r\n\r\n";
        Socket sock = getMockSocketWithString(reqString);
        Request req = ServiceFactory.createRequest(sock, null, false, null, null);
        assertEquals(id, req.session().id());
    }
}

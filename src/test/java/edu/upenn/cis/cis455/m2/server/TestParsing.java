package edu.upenn.cis.cis455.m2.server;

import edu.upenn.cis.cis455.ServiceFactory;
import edu.upenn.cis.cis455.WebServiceController;
import edu.upenn.cis.cis455.handlers.Route;
import edu.upenn.cis.cis455.m1.server.interfaces.HttpRequestHandler;
import edu.upenn.cis.cis455.m2.server.interfaces.Request;
import edu.upenn.cis.cis455.m2.server.interfaces.Response;
import org.junit.Before;
import org.junit.Test;

import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.Service;
import java.io.IOException;
import java.net.Socket;
import java.nio.file.Paths;

import static edu.upenn.cis.cis455.TestHelper.getMockSocketWithString;
import static org.junit.Assert.assertEquals;

public class TestParsing {

    @Test
    public void testPOSTParseBody() throws IOException {
        String reqString =
                "POST /postRoute HTTP/1.1\r\n" +
                "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
                "Host: www.cis.upenn.edu\r\n" +
                "Accept-Language: en-us\r\n" +
                "Accept-Encoding: gzip, deflate\r\n" +
                "Content-Type: text/plain\r\n" +
                "Content-Length: 17\r\n" +
                "Cookie: name1=value1;name2=value2;name3=value3\r\n" +
                "Connection: Keep-Alive\r\n\r\n" +
                "this is test text";

        Socket sock = getMockSocketWithString(reqString);
        Request req = ServiceFactory.createRequest(sock, null, false, null, null);
        assertEquals("this is test text", req.body());
    }

    @Test
    public void testExtractQueryParams() throws IOException {
        String reqString =
                "GET /getRoute?queryParam1=5&queryParam2=6 HTTP/1.1\r\n" +
                "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
                "Host: www.cis.upenn.edu\r\n" +
                "Accept-Language: en-us\r\n" +
                "Accept-Encoding: gzip, deflate\r\n" +
                "Cookie: name1=value1;name2=value2;name3=value3\r\n" +
                "Connection: Keep-Alive\r\n\r\n";

        Socket sock = getMockSocketWithString(reqString);
        Request req = ServiceFactory.createRequest(sock, null, false, null, null);
        assertEquals("5", req.queryParams("queryParam1"));
        assertEquals("6", req.queryParams("queryParam2"));
    }

    @Test
    public void testExtractNamedParams() throws IOException {
        WebServiceController.get("/test/:name/:val", (request, response) -> "Testing");
        String reqString =
                "GET /test/john/value HTTP/1.1\r\n" +
                "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
                "Host: www.cis.upenn.edu\r\n" +
                "Accept-Language: en-us\r\n" +
                "Accept-Encoding: gzip, deflate\r\n" +
                "Cookie: name1=value1;name2=value2;name3=value3\r\n" +
                "Connection: Keep-Alive\r\n\r\n";

        Socket sock = getMockSocketWithString(reqString);
        Request req = ServiceFactory.createRequest(sock, null, false, null, null);
        Route route = ServiceFactory.getServerInstance().getRoute(req);
        assertEquals("john", req.params(":name"));
        assertEquals("value", req.params(":val"));
    }

}

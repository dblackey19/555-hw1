package edu.upenn.cis.cis455.m1.server;

import static edu.upenn.cis.cis455.TestHelper.*;
import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.net.Socket;
import java.nio.file.Paths;

import edu.upenn.cis.cis455.ServiceFactory;
import edu.upenn.cis.cis455.m1.server.interfaces.HttpRequestHandler;
import edu.upenn.cis.cis455.m2.server.interfaces.Request;
import edu.upenn.cis.cis455.m2.server.interfaces.Response;
import org.junit.Before;
import org.junit.Test;

import org.apache.logging.log4j.Level;

import javax.servlet.http.HttpServletResponse;

public class TestSpecialURLs {

    HttpRequestHandler requestHandler;

    @Before
    public void setUp() {
        org.apache.logging.log4j.core.config.Configurator.setLevel("edu.upenn.cis.cis455", Level.DEBUG);
        requestHandler = ServiceFactory.createRequestHandlerInstance(Paths.get("./www"));
    }

    @Test
    public void testControlPanel() throws IOException {
        String reqString =
                "GET /control HTTP/1.1\r\n" +
                        "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
                        "Host: www.cis.upenn.edu\r\n" +
                        "Accept-Language: en-us\r\n" +
                        "Accept-Encoding: gzip, deflate\r\n" +
                        "Cookie: name1=value1;name2=value2;name3=value3\r\n" +
                        "Connection: Keep-Alive\r\n\r\n";

        Socket sock = getMockSocketWithString(reqString);
        Request req = ServiceFactory.createRequest(sock, null, false, null, null);
        Response response = ServiceFactory.createResponse();
        requestHandler.handle(req, response);
        assertEquals("text/html", response.type());
        assertEquals(HttpServletResponse.SC_OK, response.status());

    }
}

package edu.upenn.cis.cis455.m1.server;

import static edu.upenn.cis.cis455.TestHelper.*;
import static org.junit.Assert.assertEquals;

import edu.upenn.cis.cis455.ServiceFactory;
import edu.upenn.cis.cis455.m1.server.interfaces.HttpRequestHandler;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;


public class TestHttpTaskQueue {

    HttpTaskQueue queue;
    Socket sock;
    HttpRequestHandler reqHandler;

    @Before
    public void setUp() throws IOException {
        queue = new HttpTaskQueue();
        sock = getMockSocket("test", new ByteArrayOutputStream());
        reqHandler = ServiceFactory.createRequestHandlerInstance(Paths.get("./www"));
    }

    @Test
    public void testEnqueueAddsTask() throws IOException {
        queue.enqueue(new HttpTask(getMockSocket("test", new ByteArrayOutputStream()),
                ServiceFactory.createRequestHandlerInstance(Paths.get("./www"))));
        assertEquals(1, queue.queue.size());
    }

    @Test
    public void testMultipleThreadsEnqueue() {
        Runnable r = () -> queue.enqueue(new HttpTask(sock, reqHandler));

        Thread t1 = new Thread(r);
        Thread t2 = new Thread(r);
        t1.start();
        t2.start();
        try {
            t1.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        try {
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        assertEquals(2, queue.queue.size());
    }

    @Test
    public void testMultipleEnqueuesAndDequeues() {
        queue.enqueue(new HttpTask(sock, reqHandler));
        queue.enqueue(new HttpTask(sock, reqHandler));
        assertEquals(2, queue.queue.size());

        Runnable r = () -> queue.dequeue();
        Thread t1 = new Thread(r);
        Thread t2 = new Thread(r);
        t1.start();
        t2.start();
        try {
            t1.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        try {
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        assertEquals(0, queue.queue.size());
    }

    @Test
    public void testManyEnqueuesAndDequeues() {
        Runnable r1 = () -> queue.enqueue(new HttpTask(sock, reqHandler));
        Runnable r2 = () -> queue.dequeue();

        List<Thread> threads = new LinkedList<>();

        for (int i = 0; i < 100; i++) {
            Thread t1 = new Thread(r1);
            Thread t2 = new Thread(r2);
            threads.add(t1);
            threads.add(t2);
        }

        for (Thread t : threads) {
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        assertEquals(0, queue.queue.size());
    }
}

package edu.upenn.cis.cis455.m1.server;

import static org.junit.Assert.*;
import static edu.upenn.cis.cis455.TestHelper.*;

import edu.upenn.cis.cis455.ServiceFactory;
import edu.upenn.cis.cis455.exceptions.HaltException;
import edu.upenn.cis.cis455.m2.server.interfaces.Request;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.Socket;

public class TestRequestImpl {

    @Test(expected = HaltException.class)
    public void testNoHostSpecified() throws IOException {
        String req =
                "GET /folder HTTP/1.1\r\n" +
                        "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
                        "Accept-Language: en-us\r\n" +
                        "Accept-Encoding: gzip, deflate\r\n" +
                        "Cookie: name1=value1;name2=value2;name3=value3\r\n" +
                        "Connection: Keep-Alive\r\n\r\n";
        Socket sock = getMockSocketWithString(req);
        Request request = new RequestImpl(sock);
    }

    @Test
    public void testGetRequestMethod() throws IOException {
        String req =
                "HEAD /folder HTTP/1.1\r\n" +
                        "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
                        "Accept-Language: en-us\r\n" +
                        "Host: www.cis.upenn.edu\r\n" +
                        "Accept-Encoding: gzip, deflate\r\n" +
                        "Cookie: name1=value1;name2=value2;name3=value3\r\n" +
                        "Connection: Keep-Alive\r\n\r\n";
        Socket sock = getMockSocketWithString(req);
        Request request = new RequestImpl(sock);
        assertEquals("HEAD", request.requestMethod());
    }

    @Test
    public void testHost() throws IOException {
        String req =
                "HEAD /folder HTTP/1.1\r\n" +
                        "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
                        "Accept-Language: en-us\r\n" +
                        "Host: www.cis.upenn.edu\r\n" +
                        "Accept-Encoding: gzip, deflate\r\n" +
                        "Cookie: name1=value1;name2=value2;name3=value3\r\n" +
                        "Connection: Keep-Alive\r\n\r\n";
        Socket sock = getMockSocketWithString(req);
        Request request = new RequestImpl(sock);
        assertEquals("www.cis.upenn.edu", request.host());
    }

    @Test
    public void testPort() throws IOException {
        String req =
                "HEAD /folder HTTP/1.1\r\n" +
                        "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
                        "Accept-Language: en-us\r\n" +
                        "Host: www.cis.upenn.edu:7272\r\n" +
                        "Accept-Encoding: gzip, deflate\r\n" +
                        "Cookie: name1=value1;name2=value2;name3=value3\r\n" +
                        "Connection: Keep-Alive\r\n\r\n";
        Socket sock = getMockSocketWithString(req);
        Request request = new RequestImpl(sock);
        assertEquals(7272, request.port());
    }

    @Test
    public void testURI() throws IOException {
        String req =
                "HEAD /index.html?id=1&id2=2 HTTP/1.1\r\n" +
                        "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
                        "Accept-Language: en-us\r\n" +
                        "Host: www.cis.upenn.edu:7272\r\n" +
                        "Accept-Encoding: gzip, deflate\r\n" +
                        "Cookie: name1=value1;name2=value2;name3=value3\r\n" +
                        "Connection: Keep-Alive\r\n\r\n";
        Socket sock = getMockSocketWithString(req);
        Request request = new RequestImpl(sock);
        assertEquals("/index.html", request.uri());
    }
}

package edu.upenn.cis.cis455.m1.server;

import static edu.upenn.cis.cis455.TestHelper.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import edu.upenn.cis.cis455.ServiceFactory;
import edu.upenn.cis.cis455.exceptions.HaltException;
import edu.upenn.cis.cis455.m1.server.interfaces.HttpRequestHandler;
import edu.upenn.cis.cis455.m2.server.interfaces.Request;
import edu.upenn.cis.cis455.m2.server.interfaces.Response;
import org.junit.Before;
import org.junit.Test;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.Socket;
import java.nio.file.Paths;

public class TestValidRequests {

    HttpRequestHandler requestHandler;

    @Before
    public void setUp() {
        requestHandler = ServiceFactory.createRequestHandlerInstance(Paths.get("./www"));
    }

    @Test
    public void testRequestToHostNoFileSpecified() throws IOException {
        /* Should return index.html */

        String reqString =
                "GET / HTTP/1.1\r\n" +
                "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
                "Host: www.cis.upenn.edu\r\n" +
                "Accept-Language: en-us\r\n" +
                "Accept-Encoding: gzip, deflate\r\n" +
                "Cookie: name1=value1;name2=value2;name3=value3\r\n" +
                "Connection: Keep-Alive\r\n\r\n";

        Socket sock = getMockSocketWithString(reqString);
        Request req = ServiceFactory.createRequest(sock, null, false, null, null);
        Response response = ServiceFactory.createResponse();
        requestHandler.handle(req, response);
        assertEquals("text/html", response.type());
        assertEquals(HttpServletResponse.SC_OK, response.status());
    }

    @Test
    public void testRequestIndexHtml() throws IOException {
        String reqString =
                "GET /index.html HTTP/1.1\r\n" +
                        "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
                        "Host: www.cis.upenn.edu\r\n" +
                        "Accept-Language: en-us\r\n" +
                        "Accept-Encoding: gzip, deflate\r\n" +
                        "Cookie: name1=value1;name2=value2;name3=value3\r\n" +
                        "Connection: Keep-Alive\r\n\r\n";

        Socket sock = getMockSocketWithString(reqString);
        Request req = ServiceFactory.createRequest(sock, null, false, null, null);
        Response response = ServiceFactory.createResponse();
        requestHandler.handle(req, response);
        assertEquals("text/html", response.type());
        assertEquals(HttpServletResponse.SC_OK, response.status());
    }

    @Test
    public void testImageRequest() throws IOException {
        String reqString =
                "GET /folder/pennEng.gif HTTP/1.1\r\n" +
                        "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
                        "Host: www.cis.upenn.edu\r\n" +
                        "Accept-Language: en-us\r\n" +
                        "Accept-Encoding: gzip, deflate\r\n" +
                        "Cookie: name1=value1;name2=value2;name3=value3\r\n" +
                        "Connection: Keep-Alive\r\n\r\n";

        Socket sock = getMockSocketWithString(reqString);
        Request req = ServiceFactory.createRequest(sock, null, false, null, null);
        Response response = ServiceFactory.createResponse();
        requestHandler.handle(req, response);
        assertEquals("image/gif", response.type());
        assertEquals(HttpServletResponse.SC_OK, response.status());
    }

    @Test
    public void testConnectionClosedHeader() throws IOException {
        String reqString =
                "HEAD /folder/pennEng.gif HTTP/1.1\r\n" +
                        "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
                        "Host: www.cis.upenn.edu\r\n" +
                        "Accept-Language: en-us\r\n" +
                        "Accept-Encoding: gzip, deflate\r\n" +
                        "Cookie: name1=value1;name2=value2;name3=value3\r\n" +
                        "Connection: Keep-Alive\r\n\r\n";

        Socket sock = getMockSocketWithString(reqString);
        Request req = ServiceFactory.createRequest(sock, null, false, null, null);
        Response response = ServiceFactory.createResponse();
        requestHandler.handle(req, response);
        assertTrue(response.getHeaders().contains("Connection: close"));
    }

    @Test
    public void testHeadRequestNoBody() throws IOException {
        String reqString =
                "HEAD /folder/pennEng.gif HTTP/1.1\r\n" +
                        "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
                        "Host: www.cis.upenn.edu\r\n" +
                        "Accept-Language: en-us\r\n" +
                        "Accept-Encoding: gzip, deflate\r\n" +
                        "Cookie: name1=value1;name2=value2;name3=value3\r\n" +
                        "Connection: Keep-Alive\r\n\r\n";

        Socket sock = getMockSocketWithString(reqString);
        Request req = ServiceFactory.createRequest(sock, null, false, null, null);
        Response response = ServiceFactory.createResponse();
        requestHandler.handle(req, response);
        assertTrue(response.getHeaders().contains("Connection: close"));
    }

    @Test
    public void testIfModifiedSinceYes() throws IOException {
        String reqString =
                "GET /folder/pennEng.gif HTTP/1.1\r\n" +
                        "If-Modified-Since: Friday, 31-Dec-99 23:59:59 GMT" +
                        "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
                        "Host: www.cis.upenn.edu\r\n" +
                        "Accept-Language: en-us\r\n" +
                        "Accept-Encoding: gzip, deflate\r\n" +
                        "Cookie: name1=value1;name2=value2;name3=value3\r\n" +
                        "Connection: Keep-Alive\r\n\r\n";

        Socket sock = getMockSocketWithString(reqString);
        Request req = ServiceFactory.createRequest(sock, null, false, null, null);
        Response response = ServiceFactory.createResponse();
        requestHandler.handle(req, response);
        assertEquals(HttpServletResponse.SC_OK, response.status());
    }

    @Test
    public void testIfModifiedSinceNo() throws IOException {
        String reqString =
                "GET /folder/pennEng.gif HTTP/1.1\r\n" +
                        "If-Modified-Since: Fri Dec 31 23:59:59 2020" +
                        "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
                        "Host: www.cis.upenn.edu\r\n" +
                        "Accept-Language: en-us\r\n" +
                        "Accept-Encoding: gzip, deflate\r\n" +
                        "Cookie: name1=value1;name2=value2;name3=value3\r\n" +
                        "Connection: Keep-Alive\r\n\r\n";

        Socket sock = getMockSocketWithString(reqString);
        Request req = ServiceFactory.createRequest(sock, null, false, null, null);
        Response response = ServiceFactory.createResponse();
        requestHandler.handle(req, response);
        assertEquals(HttpServletResponse.SC_NOT_MODIFIED, response.status());
    }

    @Test
    public void testIfUnModifiedSinceYes() throws IOException {
        String reqString =
                "GET /folder/pennEng.gif HTTP/1.1\r\n" +
                        "If-Unmodified-Since: Fri Dec 31 23:59:59 2020" +
                        "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
                        "Host: www.cis.upenn.edu\r\n" +
                        "Accept-Language: en-us\r\n" +
                        "Accept-Encoding: gzip, deflate\r\n" +
                        "Cookie: name1=value1;name2=value2;name3=value3\r\n" +
                        "Connection: Keep-Alive\r\n\r\n";

        Socket sock = getMockSocketWithString(reqString);
        Request req = ServiceFactory.createRequest(sock, null, false, null, null);
        Response response = ServiceFactory.createResponse();
        requestHandler.handle(req, response);
        assertEquals(HttpServletResponse.SC_OK, response.status());
    }

    @Test(expected = HaltException.class)
    public void testIfUnModifiedSinceNo() throws IOException {
        String reqString =
                "GET /folder/pennEng.gif HTTP/1.1\r\n" +
                        "If-Unmodified-Since: Friday, 31-Dec-99 23:59:59 GMT" +
                        "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
                        "Host: www.cis.upenn.edu\r\n" +
                        "Accept-Language: en-us\r\n" +
                        "Accept-Encoding: gzip, deflate\r\n" +
                        "Cookie: name1=value1;name2=value2;name3=value3\r\n" +
                        "Connection: Keep-Alive\r\n\r\n";

        Socket sock = getMockSocketWithString(reqString);
        Request req = ServiceFactory.createRequest(sock, null, false, null, null);
        Response response = ServiceFactory.createResponse();
        requestHandler.handle(req, response);
    }

}

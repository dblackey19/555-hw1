package edu.upenn.cis.cis455;

public class SampleWebApp {
    public static void main() {
        
        // Set the web server path to ./www
        WebServiceController.staticFileLocation("./www");
        
        System.out.println("Here");
        
        // At this point, the web server should handle requests for static files from ./www
        // if you open your web browser to the Preview URL
        
        
         WebServiceController.get("index.html", (request, response) -> "Hello World");
    }
}

package edu.upenn.cis.cis455;

import edu.upenn.cis.cis455.m1.server.HttpServer;
import edu.upenn.cis.cis455.m2.server.interfaces.Request;
import edu.upenn.cis.cis455.m2.server.interfaces.Response;
import edu.upenn.cis.cis455.m2.server.interfaces.WebService;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class WebServer {
    final static Logger logger = LogManager.getLogger(WebServer.class);

    public static final int DEFAULT_PORT = 8080;
    public static final String DEFAULT_ROOT = "./www";

    public static void main(String[] args) {
        org.apache.logging.log4j.core.config.Configurator.setLevel("edu.upenn.cis.cis455", Level.DEBUG);

        int port = args.length > 0 ? Integer.parseInt(args[0]) : DEFAULT_PORT;
        String root = args.length > 1 ? args[1] : DEFAULT_ROOT;

        System.out.println("Waiting to handle requests!");

        WebServiceController.staticFileLocation(root);
        WebServiceController.port(port);
        WebServiceController.awaitInitialization();
    }

}

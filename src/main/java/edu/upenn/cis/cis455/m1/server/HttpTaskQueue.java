package edu.upenn.cis.cis455.m1.server;

import java.util.Queue;
import java.util.LinkedList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Stub class for implementing the queue of HttpTasks
 */
public class HttpTaskQueue {
    final static Logger logger = LogManager.getLogger(HttpTaskQueue.class);
    
    Queue<HttpTask> queue;
    boolean stopped;
    
    public HttpTaskQueue() {
        queue = new LinkedList<>();
        stopped = false;
    }

    /**
     * Enqueues a task to be completed. Notifies workers waiting on a task.
     * @param task
     */
    public synchronized void enqueue(HttpTask task) {
        boolean wasEmpty = queue.isEmpty();
        queue.add(task);
        if (wasEmpty) {
            notify();
        }
    }

    /**
     * Dequeue the next task. Waits until one becomes available
     * @return
     */
    public synchronized HttpTask dequeue() {
        while (!stopped) {
            if (!queue.isEmpty()) {
                return queue.poll();
            } else {
                try {
                    wait();
                } catch (InterruptedException e) {
                    /* try again */
                }
            }
        }
        return null;
    }

    /**
     * Stop threads from getting more tasks
     */
    public synchronized void stop() {
        stopped = true;
    }
}

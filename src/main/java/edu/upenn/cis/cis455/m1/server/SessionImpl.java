package edu.upenn.cis.cis455.m1.server;

import edu.upenn.cis.cis455.ServiceFactory;
import edu.upenn.cis.cis455.m2.server.interfaces.Session;

import javax.xml.ws.Service;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Set;

/**
 * Implementation of session
 */
public class SessionImpl extends Session {
    final static int DEFAULT_MAX_INACTIVE_INTERVAL = 1440;

    private String id;
    private Map<String, Object> attributes;
    private long creationTime;  // in milliseconds since 1/1/1970
    private long lastAccessedTime;  // in milliseconds since 1/1/1970
    private int maxInactiveInterval = DEFAULT_MAX_INACTIVE_INTERVAL;  // max time in seconds to keep session alive

    public SessionImpl() {
        id = generateRandomAlphanumericID((new Random()).nextInt(10) + 6);
        creationTime = System.currentTimeMillis();
        lastAccessedTime = creationTime;
        attributes = new HashMap<>();
    }

    /**
     * Generates a random alphanumeric session ID string of the specified target length
     * @return
     */
    private static String generateRandomAlphanumericID(int targetLength) {
        Random random = new Random();
        StringBuilder res = new StringBuilder();

        for (int i = 0; i < targetLength; i++) {
            int next = random.nextBoolean() ? random.nextInt('9' - '0' + 1) + '0' : random.nextInt('z' - 'a' + 1) + 'a';
            res.append((char) next);
        }

        return res.toString();
    }

    @Override
    public String id() {
        return id;
    }

    @Override
    public long creationTime() {
        return creationTime;
    }

    @Override
    public long lastAccessedTime() {
        return lastAccessedTime;
    }

    @Override
    public void invalidate() {
        attributes = null;
        ServiceFactory.getServerInstance().removeSession(id);
    }

    @Override
    public int maxInactiveInterval() {
        return maxInactiveInterval;
    }

    @Override
    public void maxInactiveInterval(int interval) {
        maxInactiveInterval = interval;
    }

    @Override
    public void access() {
        lastAccessedTime = System.currentTimeMillis();
    }

    @Override
    public void attribute(String name, Object value) {
        synchronized (this) {
            attributes.put(name, value);
        }
    }

    @Override
    public Object attribute(String name) {
        synchronized (this) {
            return attributes.get(name);
        }
    }

    @Override
    public Set<String> attributes() {
        synchronized (this) {
            return attributes.keySet();
        }
    }

    @Override
    public void removeAttribute(String name) {
        synchronized (this) {
            attributes.remove(name);
        }
    }
}

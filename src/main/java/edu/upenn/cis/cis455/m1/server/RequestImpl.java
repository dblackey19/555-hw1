package edu.upenn.cis.cis455.m1.server;

import edu.upenn.cis.cis455.ServiceFactory;
import edu.upenn.cis.cis455.exceptions.HaltException;
import edu.upenn.cis.cis455.m2.server.interfaces.Request;
import edu.upenn.cis.cis455.m2.server.interfaces.Session;
import edu.upenn.cis.cis455.util.HttpParsing;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.Socket;
import java.util.*;

/**
 * Implementation of container for HTTP request
 */
public class RequestImpl extends Request {
    final static Logger logger = LogManager.getLogger(HttpParsing.class);

    String uri, ip, body, queryString;
    boolean keepAlive;  // currently not supporting persistent connections
    Session session;
    Map<String, String> headers;
    Map<String, List<String>> params;  // query params
    Map<String, String> namedParams;
    Map<String, String> cookies;
    Map<String, Object> attributes;


    public RequestImpl(Socket sock) throws HaltException {
        ip = sock.getRemoteSocketAddress().toString();
        try {
            InputStream inputStream = sock.getInputStream();
            parseRequest(inputStream);
        } catch (IOException e) {
            logger.error("Exception encountered in reading socket input stream");
        }
    }

    public RequestImpl(Socket sock, String uri,
                       boolean keepAlive,
                       Map<String, String> headers,
                       Map<String, List<String>> params) throws HaltException {
        this.uri = uri;
        this.ip = sock.getRemoteSocketAddress().toString();
        this.keepAlive = keepAlive;
        this.headers = headers;
        this.params = params;
        try {
            InputStream inputStream = sock.getInputStream();
            parseRequest(inputStream);
        } catch (IOException e) {
            logger.error("Exception encountered in reading socket input stream");
        }
    }

    /**
     * Parses request and sets fields
     */
    void parseRequest(InputStream inputStream) throws HaltException {
        headers = headers == null ? new HashMap<>() : headers;
        params = params == null ? new HashMap<>() : params;
        cookies = new HashMap<>();
        attributes = new HashMap<>();
        namedParams = new HashMap<>();
        try {
            // disregard query string
            String uri = HttpParsing.parseRequest(ip, new BufferedInputStream(inputStream), headers, params, this);
            String[] splitQuery = uri.split("\\?");
            this.uri = splitQuery[0];
            this.queryString = splitQuery.length > 1 ? splitQuery[1] : "";
        } catch (IOException e) {
            logger.error("Error parsing request");
            throw new HaltException(HttpServletResponse.SC_BAD_REQUEST, "Error parsing request");
        }

        if (!headers.containsKey("host")) {
            logger.info("no host key");
            throw new HaltException(HttpServletResponse.SC_BAD_REQUEST, "Require host header");
        }

        parseCookies();
        String sessionId = cookies.get("JSESSIONID");
        this.session = ServiceFactory.getSession(sessionId);
    }

    /**
     * Fills cookies map with names and values from cookies header
     */
    void parseCookies() {
        if (!headers.containsKey("cookie")) {
            return;
        }

        String cookieString = headers.get("cookie").replaceAll("\\s", "");
        String[] cookiePairs = cookieString.split(";");
        for (int i = 0; i < cookiePairs.length; i++) {
            if (cookiePairs[i].isEmpty()) {
                continue;
            }
            int splitIdx = cookiePairs[i].indexOf('=');
            if (splitIdx < 0) {
                /* invalid form */
                continue;
            }
            cookies.put(cookiePairs[i].substring(0, splitIdx), cookiePairs[i].substring(splitIdx + 1));
        }
    }

    /**
     * Set the body for the request
     */
    public void setBody(String body) {
        this.body = body;
    }

    @Override
    public void setParams(Map<String, String> params) {
        this.namedParams = params;
    }

    @Override
    public String requestMethod() {
        return headers.get("Method");
    }

    @Override
    public String host() {
        return headers.get("host");
    }

    @Override
    public String userAgent() {
        return headers.get("user-agent");
    }

    @Override
    public int port() {
        String[] splitColon = host().split(":");
        return splitColon.length > 1 ? Integer.parseInt(splitColon[1]) : DEFAULT_PORT;
    }

    @Override
    public String pathInfo() {
        return uri;
    }

    @Override
    public String url() {
        return host() + uri;
    }

    @Override
    public String uri() {
        return uri;
    }

    @Override
    public String protocol() {
        return headers.get("protocolVersion");
    }

    @Override
    public String contentType() {
        return headers.get("content-type");
    }

    @Override
    public String ip() {
        return ip;
    }

    @Override
    public String body() {
        return body == null ? "" : body;
    }

    @Override
    public int contentLength() {
        if (!headers.containsKey("content-length")) {
            return 0;
        }

        int contentLength = 0;
        try {
            contentLength = Integer.parseInt(headers.get("content-length"));
        } catch (NumberFormatException e) {
            logger.info("Improperly formatted content length in request");
        }
        return contentLength;
    }

    @Override
    public String headers(String name) {
        return headers.get(name);
    }

    @Override
    public Set<String> headers() {
        Set<String> allHeaders = new HashSet<>();
        for (Map.Entry<String, String> header : headers.entrySet()) {
            allHeaders.add(header.getKey() + ": " + header.getValue());
        }
        return allHeaders;
    }

    @Override
    public Session session() {
        if (session == null) {
            return null;
        }
        session.access();
        return session;
    }

    @Override
    public Session session(boolean create) {
        if (create) {
            Session session = ServiceFactory.getSession(ServiceFactory.createSession());
            this.session = session;
            return session;
        }
        return session();
    }

    @Override
    public Map<String, String> params() {
        return namedParams;
    }

    @Override
    public String queryParams(String param) {
        if (params.get(param) == null) {
            return null;
        }
        return params.get(param).get(0);
    }

    @Override
    public List<String> queryParamsValues(String param) {
        return params.get(param);
    }

    @Override
    public Set<String> queryParams() {
        return params.keySet();
    }

    @Override
    public String queryString() {
        return queryString;
    }

    @Override
    public void attribute(String attrib, Object val) {
        attributes.put(attrib, val);
    }

    @Override
    public Object attribute(String attrib) {
        return attributes.get(attrib);
    }

    @Override
    public Set<String> attributes() {
        return attributes.keySet();
    }

    @Override
    public Map<String, String> cookies() {
        return cookies;
    }
}

package edu.upenn.cis.cis455.m1.server;

import edu.upenn.cis.cis455.ServiceFactory;
import edu.upenn.cis.cis455.exceptions.HaltException;
import edu.upenn.cis.cis455.handlers.Filter;
import edu.upenn.cis.cis455.handlers.Route;
import edu.upenn.cis.cis455.m1.server.interfaces.HttpRequestHandler;
import edu.upenn.cis.cis455.m2.server.interfaces.Request;
import edu.upenn.cis.cis455.m2.server.interfaces.Response;
import edu.upenn.cis.cis455.util.HttpParsing;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class HttpRequestHandlerImpl implements HttpRequestHandler {
    final static Logger logger = LogManager.getLogger(HttpRequestHandler.class);
    final static String SHUTDOWN = "shutdown";
    final static String CONTROL = "control";

    Path root;

    public HttpRequestHandlerImpl(Path root) {
        this.root = root;
        try {
            this.root = root.toRealPath();
        } catch (IOException e) {
            logger.error("Error setting root in request handler");
        }
    }

    byte[] readFile(Path path) throws HaltException {
        try {
            return Files.readAllBytes(path);
        } catch (IOException e) {
            logger.error("Exception encountered in opening file");
            throw new HaltException(HttpServletResponse.SC_NOT_FOUND, "File not found");
        }
    }

    /**
     * Produces the control panel for a control request with list of threads
     * in thread pool, their status (waiting or the URI they are processing),
     * a button that shuts down the server, and an error log as well
     * @return panel
     */
    String getControlScreen(Request request) {
        String threadStatus = ServiceFactory.getServerInstance().getThreadsStatus();
        List<String> errors = new ArrayList<>();
        try {
            errors = Files.readAllLines(Paths.get("./error-log.log"));
        } catch (IOException e) {
            /* nothing to do here */
        }
        StringBuilder errorLogs = new StringBuilder();
        for (String error : errors) {
            errorLogs.append("<p>" + error + "</p>");
        }
        return "<html><body>" +
                "<h2>Control Panel</h2>" +
                threadStatus +
                "<h2>Server Error Log</h2>" +
                errorLogs.toString() +
                "<a href=\"/shutdown\">" +
                "Shutdown" +
                "</a>" +
                "</body></html>";
    }

    @Override
    public void handle(Request request, Response response) throws HaltException {
        Path requestPath = Paths.get(root.toString(), request.pathInfo());
        Path realRequestPath;
        try {
            realRequestPath = requestPath.toRealPath();
        } catch (IOException e) {
            /* requested file couldn't be opened */
            realRequestPath = null;
        }

        response.status(HttpServletResponse.SC_OK);

        /* special URLs */
        if (request.pathInfo().replaceAll("/", "").equals(CONTROL)) {
            /* handle control request */

            response.body(getControlScreen(request));
            response.type("text/html");
            response.header("Content-Type", "text/html");
        } else if (request.pathInfo().replaceAll("/", "").equals(SHUTDOWN)) {
            /* handle shut down request */
            logger.info("Request to shutdown server made");
            response.body("Success: server shut down successfully");
            response.type("text/plain");
            response.header("Content-Type", "text/plain");
            ServiceFactory.getServerInstance().stop();  // stop the server
        } else {
            /* non-shutdown/control paths */

            // TODO: if redirect is called, either set HTTP code to redirect, or use their status code (if specified)
            // TODO: if redirect status code not set or set to 303, add Location header with path
            // TODO: set cookies based on what was set in response (actually this should have already been done)
            // TODO: set session ID if client called session or if original request had a session, also set the session attributes if any have been set
            // TODO: if body is still null (not set in handler) set it to what it returns

            // run before filters
            List<Filter> beforeFilters = ServiceFactory.getServerInstance()
                                            .getBeforeFilters(request);

            for (Filter f : beforeFilters) {
                try {
                    f.handle(request, response);
                } catch (HaltException e) {
                    throw e;
                } catch (Exception e) {
                    logger.error("Filter handler produced an error");
                    throw new HaltException(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                }
            }

            Route route = ServiceFactory.getServerInstance().getRoute(request);
            if (route != null) {
                // set named params in request
                try {
                    Object body = route.handle(request, response);
                    if (response.bodyRaw() == null) {
                        response.body(body.toString());
                    }
                } catch (HaltException e) {
                    throw e;
                } catch (Exception e) {
                    logger.error("Route handler produced an error");
                    throw new HaltException(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                }
            } else if (realRequestPath == null || Files.notExists(requestPath)) {
                /* file does not exist */
                logger.info("Client requested file that does not exist");
                throw new HaltException(HttpServletResponse.SC_NOT_FOUND, "Invalid request given: File not found");
            } else {
                /* retrieve static file */

                // check that the path is not an absolute path or outside static file root directory
                if (!realRequestPath.startsWith(root)) {
                    throw new HaltException(HttpServletResponse.SC_FORBIDDEN, "Invalid request given: Illegal path requested");
                }

                if (request.host().equals(request.url().replaceAll("/", ""))) {
                    // look for index.html (content not specified)
                    requestPath = Paths.get(requestPath.toString(), "index.html");  // resolve?
                }

                if (Files.isDirectory(requestPath)) {
                    throw new HaltException(HttpServletResponse.SC_NOT_FOUND, "Invalid request given: No file specified");
                }

                /* found file */

                String modifiedDate;
                boolean isHeaderIfModifiedSince = false, shouldSendBody = true;
                if ((modifiedDate = request.headers("if-modified-since")) != null && (isHeaderIfModifiedSince = true) ||
                        (modifiedDate = request.headers("if-unmodified-since")) != null) {
                    List<String> possibleDateFormats = Arrays.asList("EEE, dd MMM yyyy HH:mm:ss z",
                            "EEEEE, dd-MMM-yy HH:mm:ss z",
                            "EEE MMM dd HH:mm:ss yyyy");
                    Date date = null;
                    for (String format : possibleDateFormats) {
                        try {
                            date = new SimpleDateFormat(format).parse(modifiedDate);
                        } catch (ParseException e) {
                            /* nothing to do here */
                        }
                    }

                    if (date != null) {
                        // compare to last modified data of file
                        Date fileLastModified = null;
                        fileLastModified = new Date(new File(requestPath.toString()).lastModified());

                        if (isHeaderIfModifiedSince && date.after(fileLastModified)) {
                            /* file has not been modified since the given date, don't return */
                            response.status(HttpServletResponse.SC_NOT_MODIFIED);
                            shouldSendBody = false;
                        } else if (!isHeaderIfModifiedSince && date.before(fileLastModified)) {
                            /* file has been modified since the given data, send exception */
                            throw new HaltException(HttpServletResponse.SC_PRECONDITION_FAILED, "File");
                        }
                    }
                }

                if (request.requestMethod().equals(Request.GET) && shouldSendBody) {
                    response.bodyRaw(readFile(requestPath));  // read contents to body for GET requests
                    response.type(HttpParsing.getMimeType(requestPath.toString()));
                    response.header("Content-Type", HttpParsing.getMimeType(requestPath.toString()));
                }
            }

            List<Filter> afterFilters = ServiceFactory.getServerInstance().getAfterFilters(request);
            for (Filter f : afterFilters) {
                try {
                    f.handle(request, response);
                } catch (HaltException e) {
                    throw e;
                } catch (Exception e) {
                    logger.error("Filter handler produced an error");
                    throw new HaltException(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                }
            }
        }

        if (response.bodyRaw() != null) {
            response.header("Content-Length", response.bodyRaw().length + "");
        }
        response.header("Connection", "close");
        response.header("Date", DateTimeFormatter.RFC_1123_DATE_TIME.format(ZonedDateTime.now()));
    }
}

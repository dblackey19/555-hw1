package edu.upenn.cis.cis455.m1.server;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

/**
 * Container for a collection of HttpWorkers
 */
public class HttpWorkerPool {
    final static int NUM_THREADS = 100;

    List<HttpWorker> workers;

    public HttpWorkerPool(HttpTaskQueue requestQueue, HttpServer server) {
        this(requestQueue, server, NUM_THREADS);
    }

    public HttpWorkerPool(HttpTaskQueue requestQueue, HttpServer server, int numThreads) {
        workers = new ArrayList<>();
        for (int i = 0; i < numThreads; i++) {
            workers.add(new HttpWorker(requestQueue, server));
        }
        for (HttpWorker worker : workers) {
            worker.start();
        }
    }

    public List<HttpWorker> getPool() {
        return workers;
    }

    /**
     * Shut down all workers in the pool gracefully
     */
    public void shutDown() {
        for (HttpWorker worker : workers) {
            worker.interrupt();
        }
    }
}

package edu.upenn.cis.cis455.m1.server;

import edu.upenn.cis.cis455.ServiceFactory;
import edu.upenn.cis.cis455.exceptions.HaltException;
import edu.upenn.cis.cis455.m2.server.interfaces.Request;
import edu.upenn.cis.cis455.m2.server.interfaces.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.Socket;
import java.util.HashMap;

/**
 * Class for a thread worker for
 * handling Web requests
 */
public class HttpWorker extends Thread {
    final static Logger logger = LogManager.getLogger(HttpWorker.class);

    HttpTaskQueue requestQueue;
    HttpServer server;
    HttpTask task;
    Request request;

    public HttpWorker(HttpTaskQueue requestQueue, HttpServer server) {
        this.requestQueue = requestQueue;
        this.server = server;
        this.request = null;
    }

    @Override
    public void run() {
        // wait for a task
        while (!isInterrupted()) {
            task = requestQueue.dequeue();
            if (task == null) {
                continue;
            }

            server.start(this);  // notify manager we started a task

            Socket s = task.getSocket();
            Response response;
            try {
                request = ServiceFactory.createRequest(task.getSocket(),
                        null, false, new HashMap<>(), new HashMap<>());
                response = ServiceFactory.createResponse();
                task.getHandler().handle(request, response);
            } catch (HaltException e) {
                HttpIoHandler.sendException(s, request, e);
                completeTask();
                continue;
            }
            if (response != null) {
                HttpIoHandler.sendResponse(s, request, response);
            }
            completeTask();
        }
    }

    /**
     * Mark the task as complete.
     */
    void completeTask() {
        try {
            task.getSocket().close();
        } catch (IOException e) {
            logger.error("Error closing client socket");
        }
        task = null;
        server.done(this);
        logger.info("Completed task");
    }

    /**
     * Get the URL this worker is currently working on (or null if no task currently being executed)
     * @return
     */
    public String getURL() {
        if (!getStatus() || request == null) {
            return null;
        }
        return request.url();
    }

    /**
     * Status of this worker. Returns true if occupied with a task and
     * false otherwise.
     */
    public boolean getStatus() {
        return task != null;
    }

}

package edu.upenn.cis.cis455.m1.server;

import edu.upenn.cis.cis455.ServiceFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.channels.*;
import java.nio.file.Paths;

/**
 * HTTP server, which
 * listens on a ServerSocket and handles
 * requests
 */
public class HttpServer extends Thread implements ThreadManager {
    final static Logger logger = LogManager.getLogger(HttpServer.class);

    HttpTaskQueue requestQueue;
    HttpWorkerPool pool;
    int numThreads, port;
    String root, ip;
    boolean isActive;
    ServerSocketChannel sock;

    public HttpServer(int port, String root) {
        this.port = port;
        this.root = root;
        requestQueue = new HttpTaskQueue();
        isActive = false;
    }

    public HttpServer(int port, String root, int numThreads, String ip) {
        this.port = port;
        this.root = root;
        this.numThreads = numThreads;
        this.ip = ip;
        requestQueue = new HttpTaskQueue();
        isActive = false;
    }

    public void run() {
        init();
    }

    /**
     * Initialize the HTTP server to listen for requests
     */
    void init() {
        if (numThreads > 0) {
            /* number of threads specified */
            pool = new HttpWorkerPool(requestQueue, this, numThreads);
        } else {
            pool = new HttpWorkerPool(requestQueue, this);
        }

        try {
            isActive = true;
            sock = ServerSocketChannel.open();
            sock.socket().bind(new InetSocketAddress(ip != null
                    ? InetAddress.getByAddress(ip.getBytes())
                    : InetAddress.getLocalHost(), port));
            sock.configureBlocking(false);
            requestLoop();
        } catch (IOException e) {
            logger.error("Unable to establish server socket");
            System.exit(0);
        }
    }

    /**
     * Loop to listen for requests and invoke handlers when necessary
     */
    void requestLoop() {
        while (!isInterrupted() && isActive()) {
            SocketChannel sc = null;
            try {
                sc = sock.accept();
            } catch (ClosedByInterruptException e) {
                shutdown();
            } catch (IOException e) {
                logger.info("Exception in waiting for next client socket");
            }
            if (sc != null) {
                logger.info("Enqueueing task");
                requestQueue.enqueue(new HttpTask(sc.socket(),
                        ServiceFactory.createRequestHandlerInstance(Paths.get(root))));
            }
            if (isInterrupted()) {
                shutdown();
            }
        }
    }

    public String getRoot() {
        return root;
    }

    public void root(String root) {
        this.root = root;
    }

    /**
     * Checks if all threads are finished, and interrupts those that have
     * @return true if complete, false otherwise
     */
    boolean threadsComplete() {
        for (HttpWorker worker : pool.getPool()) {
            if (worker.getStatus()) {
                return false;
            }
        }
        logger.info("All threads complete!");
        return true;
    }

    /**
     * Shuts down the server
     */
    public void shutdown() {
        isActive = false;
        requestQueue.stop();  // prevent threads from waiting on more requests

        // exit after checking that all threads have finished
        while (!threadsComplete()) {
            try {
                sleep(5);
            } catch (InterruptedException e) {
                logger.info("Server interrupted in waiting for threads to complete");
            }
        }

        pool.shutDown();

        logger.info("Server shut down");
        System.exit(0);
    }

    /**
     * Returns threads and their status (waiting or URI) in a table
     * @return
     */
    public String getThreadsStatus() {
        StringBuilder res = new StringBuilder();
        res.append("<table style=\"width:80\\%\">");
        res.append("<tr><th>Thread</th><th>Status</th></tr>");
        for (HttpWorker worker : pool.getPool()) {
            res.append("<tr>");
            if (worker.getStatus() && worker.getURL() != null) {
                res.append("<td>" + worker + "</td><td>" + worker.getURL() + "</td>");
            } else {
                res.append("<td>" + worker + "</td><td>Waiting...</td>");
            }
            res.append("</tr>");
        }
        res.append("</table>");
        return res.toString();
    }

    /**
     * Close server socket
     * @return
     */
    public void closeSocket() throws IOException {
        logger.info("Closing socket");
        this.sock.close();
        logger.info("Closed socket");
    }

    @Override
    public HttpTaskQueue getRequestQueue() {
        return requestQueue;
    }

    @Override
    public boolean isActive() {
        return isActive;
    }

    @Override
    public void start(HttpWorker worker) {
        logger.info("Worker: " + worker + " started task");
    }

    @Override
    public void done(HttpWorker worker) {
        logger.info("Worker: " + worker + " finished task");
    }

    @Override
    public void error(HttpWorker worker) {
        logger.info("Error reported from worker", worker);
    }
}
package edu.upenn.cis.cis455.m1.server;

import edu.upenn.cis.cis455.exceptions.HaltException;
import edu.upenn.cis.cis455.m2.server.interfaces.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletResponse;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Implementation of container for HTTP response
 */
public class ResponseImpl extends Response {
    final static Logger logger = LogManager.getLogger(ResponseImpl.class);

    Map<String, String> headers;
    Map<String, String> cookies;

    public ResponseImpl() {
        headers = new HashMap<>();
        cookies = new HashMap<>();
    }

    @Override
    public String getHeaders() {
        StringBuilder headersBuilder = new StringBuilder();
        for(Map.Entry<String, String> entry : headers.entrySet()) {
            headersBuilder.append(entry.getKey() + ": " + entry.getValue() + "\r\n");
        }
        return headersBuilder.toString();
    }

    @Override
    public void header(String key, String value) {
        headers.put(key, value);
    }

    @Override
    public void redirect(String location) {
        throw new HaltException(HttpServletResponse.SC_MOVED_PERMANENTLY, "Location: " + location);
    }

    @Override
    public void redirect(String location, int httpStatusCode) {
        throw new HaltException(httpStatusCode, "Location: " + location);
    }

    @Override
    public void cookie(String name, String value) {
        cookies.put(name, name + "=" + value);
    }

    @Override
    public void cookie(String name, String value, int maxAge) {
        cookies.put(name, name + "=" + value + "; Max-Age=" + maxAge);
    }

    @Override
    public void cookie(String name, String value, int maxAge, boolean secured) {
        cookies.put(name, name + "=" + value + "; Max-Age=" + maxAge + (secured ? "; Secure" : ""));
    }

    @Override
    public void cookie(String name, String value, int maxAge, boolean secured, boolean httpOnly) {
        cookies.put(name, name + "=" + value + "; Max-Age=" + maxAge +
                        (secured ? "; Secure" : "") + (httpOnly ? "; HttpOnly" : ""));
    }

    @Override
    public void cookie(String path, String name, String value) {
        cookies.put(name, name + "=" + value + "; Path=" + path);
    }

    @Override
    public void cookie(String path, String name, String value, int maxAge) {
        cookies.put(name, name + "=" + value + "; Path=" + path + "; Max-Age=" + maxAge);
    }

    @Override
    public void cookie(String path, String name, String value, int maxAge, boolean secured) {
        cookies.put(name, name + "=" + value + "; Path=" + path + "; Max-Age=" + maxAge + (secured ? "; Secure" : ""));
    }

    @Override
    public void cookie(String path, String name, String value, int maxAge, boolean secured, boolean httpOnly) {
        cookies.put(name, name + "=" + value + "; Path=" + path + "; Max-Age=" + maxAge +
                        (secured ? "; Secure" : "") + (httpOnly ? "; HttpOnly" : ""));
    }

    @Override
    public void removeCookie(String name) {
        cookies.remove(name);
    }

    @Override
    public void removeCookie(String path, String name) {
        removeCookie(name);
    }

    @Override
    public Collection<String> getCookies() {
        return cookies.values();
    }
}

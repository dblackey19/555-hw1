package edu.upenn.cis.cis455.m1.server;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.cis455.exceptions.HaltException;
import edu.upenn.cis.cis455.m2.server.interfaces.Request;
import edu.upenn.cis.cis455.m2.server.interfaces.Response;
import edu.upenn.cis.cis455.util.HttpParsing;

/**
 * Handles marshalling between HTTP Requests and Responses
 */
public class HttpIoHandler {
    final static Logger logger = LogManager.getLogger(HttpIoHandler.class);
    final static String HTTP_PROTOCOL = "HTTP/1.1";

    /**
     * Get the HTML to show in the case of an exception
     * @param except
     * @return
     */
    static String getExceptionHTML(HaltException except) {
        return "<html><body>" +
                "<h2>Error: " + except.statusCode() + " " + HttpParsing.explainStatus(except.statusCode()) + "</h2>" +
                (except.body() == null ? "" : except.body()) + "</body></html>";
    }

    /**
     * Sends responses to client
     * @param socket
     * @param response
     */
    static void sendResponseToClient(Socket socket, byte[] response) {
        try {
            socket.getOutputStream().write(response);
        } catch (IOException e) {
            logger.error("Error writing to socket output stream");
        }
    }

    /**
     * Closes output stream and socket. This should be called after finishing writing response to
     * client
     */
    static void close(Socket socket) {
        try {
            socket.getOutputStream().close();
            socket.close();
        } catch (IOException e) {
            logger.error("Error closing client socket");
        }
    }

    /**
     * Sends an exception back, in the form of an HTTP response code and message.  Returns true
     * if we are supposed to keep the connection open (for persistent connections).
     */
    public static boolean sendException(Socket socket, Request request, HaltException except) {
        StringBuilder responseMessage = new StringBuilder();

        responseMessage.append(HTTP_PROTOCOL + " ");
        responseMessage.append(except.statusCode() + " ");
        responseMessage.append(HttpParsing.explainStatus(except.statusCode()) + "\r\n");
        if (except.statusCode() >= 300 && except.statusCode() < 400) {
            /* append location header for redirection */
            responseMessage.append(except.body() + "\r\n");
        }
        responseMessage.append("Date: " + DateTimeFormatter.RFC_1123_DATE_TIME.format(ZonedDateTime.now()) + "\r\n");
        responseMessage.append("Connection: close\r\n");
        responseMessage.append("Content-Type: text/html\r\n");

        String body = getExceptionHTML(except);

        responseMessage.append("Content-Length: " + body.getBytes().length + "\r\n\r\n");
        responseMessage.append(body);

        // send to client
        sendResponseToClient(socket, responseMessage.toString().getBytes());
        close(socket);
        return false;  // persistent connection not currently supported
    }

    /**
     * Sends data back.   Returns true if we are supposed to keep the connection open (for 
     * persistent connections).
     */
    public static boolean sendResponse(Socket socket, Request request, Response response) {
        StringBuilder responseMessage = new StringBuilder();

        responseMessage.append(HTTP_PROTOCOL + " ");
        responseMessage.append(response.status() + " ");
        responseMessage.append(HttpParsing.explainStatus(response.status()) + "\r\n");
        responseMessage.append(response.getHeaders());
        if (request.session() != null) {
            responseMessage.append("Set-Cookie: JSESSIONID=" + request.session().id() + "\r\n");
        }
        for (String cookie : response.getCookies()) {
            System.out.println(cookie);
            responseMessage.append("Set-Cookie: " + cookie + "\r\n");
        }
        responseMessage.append("\r\n");  // line break for body

        sendResponseToClient(socket, responseMessage.toString().getBytes());  // send headers

        if (response.bodyRaw() != null) {
            // send body
            sendResponseToClient(socket, response.bodyRaw());
        }
        System.out.println(responseMessage.toString());
        close(socket);
        return false; // persistent connection not currently supported
    }
}

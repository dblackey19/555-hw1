package edu.upenn.cis.cis455.m1.server;

import edu.upenn.cis.cis455.handlers.Filter;
import edu.upenn.cis.cis455.handlers.Route;
import edu.upenn.cis.cis455.m2.server.interfaces.Request;
import edu.upenn.cis.cis455.m2.server.interfaces.Session;
import edu.upenn.cis.cis455.m2.server.interfaces.WebService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.*;

public class WebServiceImpl extends WebService {
    final static Logger logger = LogManager.getLogger(WebServiceImpl.class);
    static WebService instance;

    private Map<String, List<RegisteredRoute>> routes; // request methods to list of registered routes
    private List<RegisteredFilter> beforeFilters;  // filters registered for before routes
    private List<RegisteredFilter> afterFilters; // filters registered for after routes

    private Map<String, Session> sessionMap; // id -> session object

    private String root;
    private int port;
    private int numThreads;
    private String ip;

    public WebServiceImpl(int port, String root) {
        this.port = port;
        this.root = root;
        routes = new HashMap<>();
        beforeFilters = new ArrayList<>();
        afterFilters = new ArrayList<>();
        sessionMap = new HashMap<>();
    }

    public static WebService getInstance(int port, String root) {
        if (instance == null) {
            instance = new WebServiceImpl(port, root);
        }
        return instance;
    }

    @Override
    public void start() {
        if (basicServer == null) {
            basicServer = new HttpServer(port, root, numThreads, ip);
        }
        if (basicServer.isActive()) {
            return;  // already started
        }
        basicServer.start();
    }

    @Override
    public void stop() {
        logger.info("Stopping the server");
        try {
            basicServer.interrupt();
            basicServer.closeSocket();
        } catch (IOException e) {
            logger.error("Error stopping server");
        }
    }

    /**
     * Add route to route map under method (
     *
     * @param method
     * @param path
     * @param route
     */
    void setRoute(String method, String path, Route route) {
        synchronized (routes) {
            List<RegisteredRoute> currRoutes = routes.get(method);
            if (currRoutes == null) {
                currRoutes = new ArrayList<>();
                routes.put(method, currRoutes);
            }
            currRoutes.add(new RegisteredRoute(route, path));
        }
    }

    @Override
    public Route getRoute(Request request) {
        String method = request.requestMethod();
        synchronized (routes) {
            if (routes.get(method) == null) {
                return null;
            }

            for (RegisteredRoute r : routes.get(method)) {
                if (r.doesApply(request)) {
                    return r.getRoute();
                }
            }
        }
        return null;
    }

    @Override
    public void registerSession(Session session) {
        synchronized (sessionMap) {
            sessionMap.put(session.id(), session);
        }
    }

    @Override
    public Session getSession(String sessionId) {
        synchronized (sessionMap) {
            Session session = sessionMap.get(sessionId);

            if (session == null) {
                return null;
            }
            System.out.println("Found the session");

            if (session.lastAccessedTime() < System.currentTimeMillis() - session.maxInactiveInterval() * 1000) {
                /* session expired */
                removeSession(sessionId);
                return null;
            }

            return session;
        }
    }

    @Override
    public void removeSession(String sessionId) {
        synchronized (sessionMap) {
            sessionMap.remove(sessionId);
        }
    }

    /**
     * Util function to get the appropriate list of functions based on type
     */
    private List<RegisteredFilter> getFilterList(String filterType) {
        return BEFORE.equals(filterType) ? beforeFilters : afterFilters;
    }

    /**
     * Set filter for all requests (before or after)
     * @param filterType
     * @param filter
     */
    void setFilter(String filterType, Filter filter) {
        List<RegisteredFilter> filters = getFilterList(filterType);
        synchronized (filters) {
            filters.add(new RegisteredFilter(filter));
        }
    }

    /**
     * Set filter for requests matching path/request method
     * @param path
     * @param acceptType
     * @param filter
     */
    void setFilter(String filterType, String path, String acceptType, Filter filter) {
        List<RegisteredFilter> filters = getFilterList(filterType);
        synchronized (filters) {
            filters.add(new RegisteredFilter(filter, path, acceptType));
        }
    }

    @Override
    public List<Filter> getBeforeFilters(Request request) {
        return getFilters(BEFORE, request);
    }

    @Override
    public List<Filter> getAfterFilters(Request request) {
        return getFilters(AFTER, request);
    }

    /**
     * Get all the filters that apply to a request
     * @param method
     * @param path
     * @return
     */
    List<Filter> getFilters(String filterType, Request request) {
        List<RegisteredFilter> filters = getFilterList(filterType);

        List<Filter> res = new LinkedList<>();

        synchronized (filters) {
            // check all filters and add to list
            for (RegisteredFilter f : filters) {
                if (f.doesApply(request)) {
                    res.add(f.getFilter());
                }
            }
        }
        return res;
    }

    @Override
    public void staticFileLocation(String directory) {
        this.root = directory;
    }

    @Override
    public void ipAddress(String ipAddress) {
        this.ip = ipAddress;
    }

    @Override
    public void port(int port) {
        this.port = port;
    }

    @Override
    public void threadPool(int threads) {
        this.numThreads = threads;
    }

    @Override
    public String getThreadsStatus() {
        if (basicServer == null) {
            return null;
        }
        return basicServer.getThreadsStatus();
    }

    @Override
    public void resetInstance() {
        instance = null;
    }

    @Override
    public void post(String path, Route route) {
        setRoute(POST, path, route);
    }

    @Override
    public void put(String path, Route route) {
        setRoute(PUT, path, route);
    }

    @Override
    public void delete(String path, Route route) {
        setRoute(DELETE, path, route);
    }

    @Override
    public void options(String path, Route route) {
        setRoute(OPTIONS, path, route);
    }

    @Override
    public void get(String path, Route route) {
        setRoute(GET, path, route);
    }

    @Override
    public void head(String path, Route route) {
        setRoute(HEAD, path, route);
    }

    @Override
    public void before(Filter filter) {
        setFilter(BEFORE, filter);
    }

    @Override
    public void after(Filter filter) {
        setFilter(AFTER, filter);
    }

    @Override
    public void before(String path, String acceptType, Filter filter) {
        setFilter(BEFORE, path, acceptType, filter);
    }

    @Override
    public void after(String path, String acceptType, Filter filter) {
        setFilter(AFTER, path, acceptType, filter);
    }

    /**
     * Do the two paths match? Considers wild cards and named parameters in the first path.
     *
     * @param path1
     * @param path2
     * @return
     */
    static boolean doesMatchUtil(String[] path1, String[] path2) {
        int i = 0, j = 0;
        while (i < path2.length && j < path1.length) {

            while(i < path2.length && path2[i].isEmpty()) {
                i++;
            }
            while (j < path1.length && path1[j].isEmpty()) {
                j++;
            }

            if (i >= path2.length || j >= path1.length) {
                break;
            }

            if (!path1[i].equals(path2[j]) && !path1[j].startsWith(":") && !path1[j].equals("*")) {
                /* no match */
                return false;
            }
            i++;
            j++;
        }

        while(i < path2.length && path2[i].isEmpty()) {
            i++;
        }
        while (j < path1.length && path1[j].isEmpty()) {
            j++;
        }

        return i == path2.length && j == path1.length;
    }

    /**
     * Get a map of named params based on a path match
     *
     * @param path
     * @param requestPath
     */
    static Map<String, String> getNamedParams(String[] path, String[] requestPath) {
        Map<String, String> res = new HashMap<>();
        int i = 0, j = 0;

        while (i < requestPath.length && j < path.length) {

            while(i < requestPath.length && requestPath[i].isEmpty()) {
                i++;
            }
            while (j < path.length && path[j].isEmpty()) {
                j++;
            }

            if (i >= requestPath.length || j >= path.length) {
                break;
            }

            if (path[j].startsWith(":")) {
                res.put(path[j], requestPath[i]);
            }
            i++;
            j++;
        }

        return res;
    }

    /**
     * Container for a route and the paths it should match
     */
    static class RegisteredRoute {
        final Route route;
        final String[] path;

        RegisteredRoute(Route route, String path) {
            this.route = route;
            this.path = path.split("/");
        }

        Route getRoute() {
            return this.route;
        }

        /**
         * Check if this route should apply to the path. Returns true if
         * it matches, and false otherwise. Also sets the named parameters in the request
         * if it matches.
         */
        boolean doesApply(Request request) {
            String[] toCheck = request.uri().split("/");
            boolean match = doesMatchUtil(path, toCheck);
            if (match) {
                request.setParams(getNamedParams(path, toCheck));
            }
            return match;
        }
    }

    /**
     * Container for a filter, and possibly the paths/methods it applies to
     */
    static class RegisteredFilter {
        final Filter filter;
        final String[] path;
        final String acceptType;

        RegisteredFilter(Filter filter) {
            this(filter, null, null);
        }

        RegisteredFilter(Filter filter, String path, String acceptType) {
            this.filter = filter;
            this.path = path == null ? null : path.split("/");
            this.acceptType = acceptType;
        }

        Filter getFilter() {
            return this.filter;
        }

        /**
         * Check if this filter should apply to a request with the given request method (accept type)
         * and path. Also sets the named parameters in the request if it matches.
         */
        boolean doesApply(Request request) {
            String acceptType = request.requestMethod();
            String path = request.uri();
            if (this.path == null && this.acceptType == null) {
                /* filter applies to all requests */
                return true;
            }

            if (this.path == null) {
                return this.acceptType.equals(acceptType);
            }

            String[] toCheck = path.split("/");

            boolean match = this.acceptType == null ? doesMatchUtil(this.path, toCheck)
                                    : this.acceptType.equals(acceptType) && doesMatchUtil(this.path, toCheck);

            if (match) {
                request.setParams(getNamedParams(this.path, toCheck));
            }
            return match;

        }
    }
}

package edu.upenn.cis.cis455.m1.server;

import edu.upenn.cis.cis455.m1.server.interfaces.HttpRequestHandler;
import edu.upenn.cis.cis455.m1.server.interfaces.Request;
import edu.upenn.cis.cis455.util.HttpParsing;

import java.net.Socket;

public class HttpTask {
    Socket requestSocket;
    HttpRequestHandler handler;
    String url; // for displaying thread status
    
    public HttpTask(Socket requestSocket, HttpRequestHandler handler) {
        this.requestSocket = requestSocket;
        this.handler = handler;
        // TODO: figure out how to get the URL efficiently
        // could parse request but seems like I'm already processing it?
    }
    
    public Socket getSocket() {
        return requestSocket;
    }

    public HttpRequestHandler getHandler() {
        return handler;
    }

}

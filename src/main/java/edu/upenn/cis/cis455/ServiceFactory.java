package edu.upenn.cis.cis455;

import java.net.Socket;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;

import edu.upenn.cis.cis455.m1.server.*;
import edu.upenn.cis.cis455.m2.server.interfaces.WebService;
import edu.upenn.cis.cis455.m1.server.interfaces.HttpRequestHandler;
import edu.upenn.cis.cis455.m2.server.interfaces.Request;
import edu.upenn.cis.cis455.m2.server.interfaces.Response;
import edu.upenn.cis.cis455.m2.server.interfaces.Session;


public class ServiceFactory {

    /**
     * Get the HTTP server associated with port 8080
     */
    public static WebService getServerInstance() {
        return WebServiceImpl.getInstance(WebServer.DEFAULT_PORT, WebServer.DEFAULT_ROOT);
    }
    
    /**
     * Create an HTTP request given an incoming socket
     */
    public static Request createRequest(Socket socket,
                         String uri,
                         boolean keepAlive,
                         Map<String, String> headers,
                         Map<String, List<String>> parms) {
        return new RequestImpl(socket, uri, keepAlive, headers, parms);
    }
    
    /**
     * Gets a request handler for files (i.e., static content) or dynamic content
     */
    public static HttpRequestHandler createRequestHandlerInstance(Path serverRoot) {
        return new HttpRequestHandlerImpl(serverRoot);
    }

    /**
     * Gets a new HTTP Response object
     */
    public static Response createResponse() {
        return new ResponseImpl();
    }

    /**
     * Creates a blank session ID and registers a Session object for the request
     */
    public static String createSession() {
        Session session = new SessionImpl();
        getServerInstance().registerSession(session);
        return session.id();
    }
    
    /**
     * Looks up a session by ID and updates / returns it
     */
    public static Session getSession(String id) {
        return getServerInstance().getSession(id);
    }
}